-- phpMyAdmin SQL Dump
-- version 4.1.9
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Sam 08 Novembre 2014 à 09:08
-- Version du serveur :  5.1.73
-- Version de PHP :  5.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `bld_projet`
--

-- --------------------------------------------------------

--
-- Structure de la table `adherent`
--

CREATE TABLE IF NOT EXISTS `adherent` (
  `id_user` int(11) NOT NULL,
  `num_adh` int(11) NOT NULL,
  `mdp_adh` varchar(20) NOT NULL,
  KEY `id_user` (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `adherent`
--

INSERT INTO `adherent` (`id_user`, `num_adh`, `mdp_adh`) VALUES
(2, 1599, 'toto'),
(3, 1588, 'toto'),
(4, 1212, 'titi');


-- --------------------------------------------------------


--
-- Structure de la table `doc_genre`
--

CREATE TABLE IF NOT EXISTS `doc_genre` (
  `id_doc` int(11) NOT NULL,
  `id_gen` int(11) NOT NULL,
  KEY `id_gen` (`id_gen`),
  KEY `id_doc` (`id_doc`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `doc_genre`
--

INSERT INTO `doc_genre` (`id_doc`, `id_gen`) VALUES
(3, 16),
(4, 20),
(5, 17),
(6, 28),
(1, 4),
(1, 13),
(7, 7),
(8, 9),
(8, 3),
(10, 8),
(10, 4),
(10, 13),
(12, 12),
(13, 13),
(13, 7),
(14, 14),
(14, 2),
(15, 14),
(15, 15),
(16, 2),
(16, 3),
(16, 5);

-- --------------------------------------------------------

--
-- Structure de la table `document`
--

CREATE TABLE IF NOT EXISTS `document` (
  `id_doc` int(11) NOT NULL AUTO_INCREMENT,
  `ref_doc` varchar(20) NOT NULL,
  `titre_doc` varchar(50) NOT NULL,
  `auteur_doc` varchar(20) NOT NULL,
  `date_doc` date NOT NULL,
  `desc_doc` varchar(800) NOT NULL,
  `img_doc` varchar(40) NOT NULL,
  `id_type` int(11) NOT NULL,
  `id_etat` int(11) NOT NULL,
  PRIMARY KEY (`id_doc`),
  KEY `id_type` (`id_type`),
  KEY `id_etat` (`id_etat`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Contenu de la table `document`
--

INSERT INTO `document` (`id_doc`, `ref_doc`, `titre_doc`, `auteur_doc`, `date_doc`, `desc_doc`, `img_doc`, `id_type`, `id_etat`) VALUES
(1, 'DVD0001', 'Forrest Gump', 'Robert Zemeckis', '2001-08-28', 'Quelques décennies d''histoire américaine, des années 1940 à la fin du XXème siècle, à travers le regard et l''étrange odyssée d''''un homme simple et pur, Forrest Gump. ', 'img/forrest_gump.jpg', 3, 1),
(3, 'CD0002', 'Coeur de verre', 'Helene Segara', '1996-10-23', 'Cœur de verre est le premier album d''Hélène Ségara sorti 1996 sur le label East west. Il s''est vendu à plus de 100 000 exemplaires et a été certifié disque d''or en France', 'img/coeur_de_verre.jpg', 2, 1),
(4, 'CD0001', 'L''apocalypse', 'Soprano', '2014-10-23', '', 'img/cosmopolitanie.jpg', 2, 2),
(5, 'CD0003', 'The Geeks and the Jerkin'' Socks', 'Shakaponk', '2014-10-23', 'The Geeks and the Jerkin'' Socks est le 3e album de Shaka Ponk, sorti le 6 juin 2011. Cet album voit apparaître un nouveau membre dans le groupe en la présence de Sam, qui vient apporter une voix féminine au groupe.', 'img/the_geeks_and_the_jerkin_socks.jpg', 2, 1),
(6, 'CD0004', 'Les Choristes', 'Bruno Coulais', '2004-05-14', 'Lauréat aux Victoires de la musique 2005 : album de musique originale de cinéma ou de télévision Bruno Coulais compose une nouvelle fois une musique somptueuse, tendre et nostalgique, comme le film de Gérard Jugnot, avec des voix merveilleuses, puisqu''il s''agit bien de chant choral. Comme toutes les bonnes B.O, celle-ci peut parfaitement s''écouter même si l''on n''a pas vu le film.', 'img/les_choristes.jpg', 2, 1),
(7, 'LIV0001', 'Elle s''appelait Sarah', 'Tatiana de Rosnay', '2008-06-05', 'Paris, juillet 1942 : Sarah, une fillette de dix ans qui porte l''étoile jaune, est arrêtée avec ses parents par la police française, au milieu de la nuit. Paniquée, elle met son petit frère à l''abri en lui promettant de revenir le libérer dès que possible.\nParis, mai 2002 : Julia Jarmond, une journaliste américaine mariée à un français, doit couvrir la commémoration de la rafle du Vel d"Hiv. Soixante ans après, son chemin va croiser celui de Sarah, et sa vie changer à jamais.\nELLE S''APPELAIT SARAH, c''est l''histoire de deux familles que lie un terrible secret, c''est aussi l''évocation d''une des pages les plus sombres de l''Occupation.\nUn roman bouleversant sur la culpabilité et le devoir de mémoire, qui connaît un succés international, avec des traductions dans vingt pays.', 'img/elle_s_appelait_sarah.jpg', 1, 1),
(8, 'LIV0002', 'Harry Potter I', 'J.K Rowlings', '1997-12-03', 'Depuis la mort de ses parents, Harry vit chez son oncle et sa tante, les horribles Dursmley. Le jour de ses onze ans, il reçoit une lettre, deux lettres, des milliers de lettres qu''ils lui interdisent de lire. Le géant Hagrid se déplace donc en personne pour lui apprendre qu''il est attendu à l''école des sorciers... Un phénomène de librairie qui apporte un peu de magie au quotidien. Mais attention, ce livre exerce bien un maléfice sur ses lecteurs : quand on le commence, on ne le lâche plus !', 'img/harry_potter.jpg', 1, 1),
(10, 'LIV0003', 'Carrie', 'Stephen King', '1974-04-05', 'Carrie White, dix-sept ans, solitaire, timide et pas vraiment jolie, vit un calvaire : elle est victime du fanatisme religieux de sa mère et des moqueries incessantes de ses camarades de classe. Sans compter ce don, cet étrange pouvoir de déplacer les objets à distance, bien qu?elle le maîtrise encore avec diffi culté... Un jour, cependant, la chance paraît lui sourire. Tommy Ross, le seul garçon qui semble la comprendre et l?aimer, l?invite au bal de printemps de l?école. Une marque d?attention qu?elle n?aurait jamais espérée, et peut-être même le signe d''un renouveau...', 'img/carrie.jpg', 1, 1),
(11, 'LIV0004', 'Bridget Jones', 'Helen Fielding', '1996-01-01', '"58,5 kg (mais post-Noël), unités d''alcool : 14 (mais compte en fait pour deux à cause de soirée de nouvel an), cigarettes : 22, calories : 5 422". A presque trente ans, Bridget Jones consigne ses déboires amoureux dans son journal. Elle sort trop, fume trop, boit trop, compte les calories et fantasme sur son play-boy de patron. Sa hantise : finir vieille fille. Ses objectifs : perdre du poids et trouver son prince charmant. L''irrésistible confession de la célibataire la plus célèbre de la planète.', 'img/bridget_jones.jpg', 1, 1),
(12, 'LIV0005', 'Poirot joue le jeu', 'Agatha Christie', '1956-08-26', 'L''Inspecteur commençait à juger Mrs Olivier fort injustement et son impression était fortifiée par une vague odeur de cognac. A leur retour dans la maison, Hercule Poirot avait tenu à faire absorber ce remède à sa vieille amie. Elle devina ce que pensait Bland et déclara aussitôt : "Je suis pas folle et je ne suis pas ivre, mais probablement cet individu qui affirme que je bois comme un trou vous a convaincu. -Quel individu ? demanda le policier qui passait du jardinier en second à ce personnage anonyme et n''y comprenait plus rien. -Il a des taches de rousseur et l''accent du Yorkshire, répondit Mrs. Olivier. Mais, je le répète, je ne suis ni ivre ni folle. je suis bouleversée. Absolument bouleversée", conclut-elle avec force.', 'img/poirot.jpg', 1, 3),
(13, 'DVD0002', 'Love Story', 'Arthur Hiller', '1970-06-25', 'Un homme issu d''une famille aisée, étudiant en droit à Harvard, rencontre une jeune fille à la bibliothèque où elle travaille pour payer ses études. Malgré le fait qu''ils appartiennent à des classes sociales différentes leur amour devient plus fort que tout mais sera soumis aux épreuves de la vie...', 'img/love_story.jpg', 3, 1),
(14, 'DVD0003', 'Avatar', 'James Cameron', '2009-04-08', 'Malgré sa paralysie, Jake Sully, un ancien marine immobilisé dans un fauteuil roulant, est resté un combattant au plus profond de son être. Il est recruté pour se rendre à des années-lumière de la Terre, sur Pandora, où de puissants groupes industriels exploitent un minerai rarissime destiné à résoudre la crise énergétique sur Terre. Parce que l''atmosphère de Pandora est toxique pour les humains, ceux-ci ont créé le Programme Avatar, qui permet à des " pilotes " humains de lier leur esprit à un avatar, un corps biologique commandé à distance, capable de survivre dans cette atmosphère létale. Ces avatars sont des hybrides créés génétiquement en croisant l''ADN humain avec celui des Na''vi, les autochtones de Pandora.\nSous sa forme d''avatar, Jake peut de nouveau marcher. ', 'img/avatar.jpg', 3, 1),
(15, 'DVD0004', 'Dark Skies', 'Scott Stewart', '2013-02-21', 'Dans une banlieue paisible, la famille Barrett voit soudainement sa vie basculer suite à des évènements étranges qui, chaque nuit, viennent troubler la tranquillité de sa maison. Lorsque leur fils cadet évoque un mystérieux « Ogre des sables » lui rendant visite le soir, le quotidien de Daniel et Lacy Barrett tourne alors au cauchemar : ils deviennent victimes d’inquiétants trous de mémoires, et de soudaines pertes de contrôle de leur corps. Ne trouvant aucun soutien autour d’eux, ils se retrouvent impuissants pour affronter ce qui va se révéler être une force extra-terrestre cherchant à s''emparer de leurs enfants...', 'img/dark_skies.jpg', 3, 1),
(16, 'DVD0005', 'Le roi lion', 'Disney', '1996-06-11', 'Dans la savane africaine, tous les animaux de la Terre des Lions se sont réunis pour célébrer la naissance du prince Simba, fils du roi Mufasa et de la reine Sarabi. Tous, sauf Scar, frère cadet de Mufasa, pour qui la naissance de cet héritier anéantit tous ses espoirs d''accéder un jour au pouvoir. Avec la collaboration des hyènes, Scar imagine plusieurs plans diaboliques pour anéantir son frère et son neveu.', 'img/le_roi_lion.jpg', 3, 2);

-- --------------------------------------------------------

--
-- Structure de la table `emprunt`
--

CREATE TABLE IF NOT EXISTS `emprunt` (
  `id_emp` int(11) NOT NULL AUTO_INCREMENT,
  `id_use` int(11) NOT NULL,
  `id_doc` int(11) NOT NULL,
  `date_enr` date NOT NULL,
  `date_retour` date DEFAULT NULL,
  `date_rendu` date DEFAULT NULL,
  PRIMARY KEY (`id_emp`,`id_use`,`id_doc`),
  KEY `id_doc` (`id_doc`),
  KEY `id_use` (`id_use`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=37 ;

--
-- Contenu de la table `emprunt`
--

INSERT INTO `emprunt` (`id_emp`, `id_use`, `id_doc`, `date_enr`, `date_retour`, `date_rendu`) VALUES
(35, 2, 4, '2014-11-06', '2014-12-06', NULL),
(36, 2, 16, '2014-11-06', '2014-12-06', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `etat`
--

CREATE TABLE IF NOT EXISTS `etat` (
  `id_etat` int(11) NOT NULL AUTO_INCREMENT,
  `libelle_etat` varchar(30) NOT NULL,
  PRIMARY KEY (`id_etat`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `etat`
--

INSERT INTO `etat` (`id_etat`, `libelle_etat`) VALUES
(1, 'Disponible'),
(2, 'Emprunté'),
(3, 'Indisponible');

-- --------------------------------------------------------

--
-- Structure de la table `genre`
--

CREATE TABLE IF NOT EXISTS `genre` (
  `id_gen` int(11) NOT NULL AUTO_INCREMENT,
  `libelle_gen` varchar(30) NOT NULL,
  PRIMARY KEY (`id_gen`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=29 ;

--
-- Contenu de la table `genre`
--

INSERT INTO `genre` (`id_gen`, `libelle_gen`) VALUES
(1, 'Action'),
(2, 'Animation'),
(3, 'Aventure'),
(4, 'Comedie'),
(5, 'Dessin animé'),
(6, 'Documentaire'),
(7, 'Drame'),
(8, 'Epouvante-horreur'),
(9, 'Fantastique'),
(10, 'Guerre'),
(11, 'Musical'),
(12, 'Policier'),
(13, 'Romance'),
(14, 'Science fiction'),
(15, 'Thriller'),
(16, 'Variété Française'),
(17, 'Pop, Rock, Indé'),
(18, 'Musique Classique'),
(19, 'Opéra'),
(20, 'Rap'),
(21, 'R&B, Soul, Funk'),
(22, 'Jazz, Blues'),
(23, 'Musique du Monde'),
(24, 'Electro'),
(25, 'Hard Rock, Métal'),
(26, 'Enfant'),
(27, 'Compilations'),
(28, 'BO, musiques de films');

-- --------------------------------------------------------

--
-- Structure de la table `staff`
--

CREATE TABLE IF NOT EXISTS `staff` (
  `id_use` int(11) NOT NULL,
  `date_embauche` date NOT NULL,
  KEY `id_use` (`id_use`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `staff`
--

INSERT INTO `staff` (`id_use`, `date_embauche`) VALUES
(1, '2014-11-03'),
(2, '0000-00-00');

-- --------------------------------------------------------

--
-- Structure de la table `type`
--

CREATE TABLE IF NOT EXISTS `type` (
  ` id_type` int(11) NOT NULL AUTO_INCREMENT,
  `libelle_type` varchar(30) NOT NULL,
  PRIMARY KEY (` id_type`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `type`
--

INSERT INTO `type` (` id_type`, `libelle_type`) VALUES
(1, 'Livre'),
(2, 'CD'),
(3, 'DVD');

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

CREATE TABLE IF NOT EXISTS `utilisateur` (
  `id_use` int(11) NOT NULL AUTO_INCREMENT,
  `nom_use` varchar(20) NOT NULL,
  `prenom_use` varchar(20) NOT NULL,
  `adresse_use` varchar(30) DEFAULT NULL,
  `cp_use` int(11) DEFAULT NULL,
  `ville_use` varchar(30) DEFAULT NULL,
  `date_naiss_use` date DEFAULT NULL,
  `tel_use` varchar(20) DEFAULT NULL,
  `mail_use` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id_use`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Contenu de la table `utilisateur`
--

INSERT INTO `utilisateur` (`id_use`, `nom_use`, `prenom_use`, `adresse_use`, `cp_use`, `ville_use`, `date_naiss_use`, `tel_use`, `mail_use`) VALUES
(1, 'Dupont', 'Sylvie', ' 15 rue du pont', 54000, 'Nancy', '1965-12-08', '0123456789', 'dupontsylvie54@laposte.net'),
(2, 'Dupond', 'Michel', '114 rue Jeanne d''Arc', 54000, 'Nancy', '1970-11-11', '0987654321', 'mimichdu54@sfr.fr'),
(3, 'toto', 'toto', 'totoadresse', 55170, 'totocity', '2014-11-05', '0508090808', 'toto@gmail.com'),
(4, 'titi', 'titi', 'titiadresse', 54780, 'titicity', '2014-11-01', '0205098745', 'titi@gmail.com');

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `adherent`
--
ALTER TABLE `adherent`
  ADD CONSTRAINT `adherent_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `utilisateur` (`id_use`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `doc_genre`
--
ALTER TABLE `doc_genre`
  ADD CONSTRAINT `doc_genre_ibfk_1` FOREIGN KEY (`id_doc`) REFERENCES `document` (`id_doc`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `doc_genre_ibfk_2` FOREIGN KEY (`id_gen`) REFERENCES `genre` (`id_gen`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `document`
--
ALTER TABLE `document`
  ADD CONSTRAINT `document_ibfk_1` FOREIGN KEY (`id_type`) REFERENCES `type` (` id_type`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `document_ibfk_2` FOREIGN KEY (`id_etat`) REFERENCES `etat` (`id_etat`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `emprunt`
--
ALTER TABLE `emprunt`
  ADD CONSTRAINT `emprunt_ibfk_1` FOREIGN KEY (`id_use`) REFERENCES `utilisateur` (`id_use`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `emprunt_ibfk_2` FOREIGN KEY (`id_doc`) REFERENCES `document` (`id_doc`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `staff`
--
ALTER TABLE `staff`
  ADD CONSTRAINT `staff_ibfk_1` FOREIGN KEY (`id_use`) REFERENCES `utilisateur` (`id_use`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
