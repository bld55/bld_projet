<?php
function __autoload($class_name){
	require_once("Class/".$class_name.".php");

}
session_start();

if(isset($_GET['action'])){
	$action=$_GET['action'];
	// tab de toutes les actions possible côté staff
	$action_gs = array('enremprunt','newemprunt','retourEmprunt','pagerecapenr');
	// booléen faux par défaut

	$b = false;
	// parcours le tableau. Si l'action correspond à quelque chose du tableau, le boolean passe à true
	foreach($action_gs as $a){
		if($a == $action){
			$b=true;
		}
	}

	// si le booleen est en true, redirection coté staff
	if($b==true){
		$gs= new C_GestionStaff();
		$gs->dispatch($_REQUEST);
	}
}
// si action non définie, redirection accueil
else{
	header('Location:index.php?action=accueil');
}

// redirection adhérent par défaut
$ga = new C_GestionAdherent();
$ga->dispatch($_REQUEST);
?>