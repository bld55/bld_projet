<?php

class View {

 private $middle;

 private $document;

 private $livre;

 private $cd;

 private $dvd;

 private $emprunt;

 private $genre;

 private $user;

 private $doc_genre;

 private $etat;

 public function __construct() {

 }

 public function __get($attr_name) {
  if (property_exists( __CLASS__, $attr_name)) { 
    return $this->$attr_name;
  } 
  $emess = __CLASS__ . ": unknown member $attr_name (getAttr)";
  throw new Exception($emess, 45);
}

public function __set($attr_name, $attr_val) {
  if (property_exists( __CLASS__, $attr_name)) {
    $this->$attr_name=$attr_val; 
    return $this->$attr_name;
  } 
  $emess = __CLASS__ . ": unknown member $attr_name (setAttr)";
  throw new Exception($emess, 45);
}

public function affichHeader(){
  $resu='<!DOCTYPE html>
  <html>
  <head>
  <link href="css/grille1.css" rel="stylesheet" type="text/css" />
  <link href="css/framework_lj.css" rel="stylesheet" type="text/css" />
  <link href="css/style.css" rel="stylesheet" type="text/css" />
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <title>mediaNet - Nancy</title>
  </head>

  <body>
  <header>
  <div id="bandeau" class="col no_margin span-12 centrer uppercase">
  <h1>mediaNet <br><small>Médiathèque de la ville de Nancy</small></h1>
  </div>

  
  </header><div id="corps">';
  return $resu;
}

public function affichNavbarDefaut(){
  $resu='<div id="navbar" class="col span-12 sans_deco centrer nomarginright">
  <strong>
  <ul class="no_padding ligne">
  <li class="sans_deco no_margin col span-6">
  <a href="index.php?action=catalogue">Catalogue</a>
  </li>
  <li class="sans_deco no_margin col span-6">
  <a href="index.php?action=formVoirEmprunts">Mes emprunts</a>
  </li>
  </ul>
  </strong>
  </div>';
  return $resu;
}

public function affichAccueil(){
  $resu='<div id="navbar" class="col span-12 sans_deco centrer nomarginright">
  <strong>
  <ul class="no_padding ligne">
  <li id="navbarAccueil" class="sans_deco no_margin col span-6">
  &nbsp;
  </li>
  <li id="navbarAccueil" class="sans_deco no_margin col span-6">
  &nbsp;
  </li>
  </ul>
  </strong>
  </div>
  <div id="content" class="offset-1 span-10">
  <div id="welcome">

  <h3 class="centrer">Bienvenue sur le site mediaNet</h3>
  </div>
  <div id="resu" class="col span-6 centrer nomarginright">
  <strong>Site adhérent</strong>
  <a href="index.php?action=catalogue"><input type="button" value="Accéder"></a>
  </div>
  <div id="resu" class="col span-6 centrer nomarginright">
  <strong>Site staff</strong>
  <a href="index.php?action=enremprunt"><input type="button" value="Accéder">
  </div>
  </div>';

  return $resu;
}

public function affichFooter(){
  $resu="</body>";
  return $resu;
}

public function affichMesEmprunts($emprunt){

 $resu='<div id="content" class="offset-1 span-10">
 <div id="welcome">

 <h3 class="centrer">Mes Emprunts</h3>
 </div>
 <div id="resu" class="offset-1">
 ';
 // affiche les emprunts s'ils existent, sinon affichage message d'erreur 
 if(empty($emprunt)){
  $resu.="<p class='centrer'>L'utilisateur n'est pas reconnu. <br><a href='index.php?action=formVoirEmprunts'>Retour au formulaire.</a></p>";
}else{
  $resu.='<ul>';
  foreach($emprunt as $em){
    $resu.='<li class="span-3 nomarginright">
    <span id="img"><img src="'.$em->img.'"></span>
    <span id="titre">'.$em->titre.'</span><br>
    <span id="auteur">'.$em->auteur.'</span><br>
    <span id="date">'.$em->date.'</span>
    </li>
    ';
  }
}
$resu.='</ul></div></div>';      

return $resu;
}

// affiche tous les details sur le document en question (dont etat, genre)
public function affichDocumentDetail($document,$genre,$etat,$emprunt){
  
  $resu = "<div id='content' class='offset-1 span-10'>
  <div id='welcome'>
  <h3 class='centrer'>Détails du document</h3>
  </div>
  <p class='titre_doc'> ".$document->titre."</p>
  <div class='img_detail'>
  <img class='img_detail_document' src=".$document->img.">
  </div>
  <div class='p_detail'>
  <p><b><u>Date :</u></b> ".$document->date."</p>
  <p><b><u>Auteur :</b></u>".$document->auteur."</p>
  <p><b><u>Genre : </b></u><ul id=class='puce_carree'>";
  // affichage des genres sous forme de liste
  foreach($this->genre as $g){
    $resu.="<li id='genre_doc'>".$g->libelle_gen."</li>";
  }

  $resu.="</ul></p>
  <p><b><u>Description:</b></u>".$document->desc."</p>
  <p><b><u>Etat :</b></u> ".$etat->libelle_etat."</p>";

      // Si c'est emprunté
  if($etat->id_etat == 2){
    
    $resu.="<p><b><u>Date de retour  : </b></u>".$emprunt->date_retour."</p>";
  }

  $resu.="</div>
  </div>
  </div>";
  return $resu;
}

public function affichFormVoirMesEmprunts(){

 $resu='<div id="content" class="offset-1 span-10">
 <div id="welcome">
 <h3 class="centrer">Mes Emprunts</h3>
 </div>
 <div id="resu" class="centrer">
 <h4 class="centrer">Entrez votre N° d adhérent:</h4>
 <form name="recherche_emprunt" method="POST" action="index.php?action=mesEmprunts">
 <input type="text" name="id_adh"/>
 <input type="submit" class="btn" value="valider"></button>
 </form>
 </div>
 </div>';
 return $resu;
}




public function afficheCatalogue($l, $c, $d){
  $res="";
  $res.='<div id="search">
  <form action="index.php?action=resuRech" method="POST">
  <input type="text" name="mot_rech" id="mot_rech" placeholder="Recherche" class="offset-8 span-2 nomarginright">
  <input type="submit" class="btn" id="valRecherche" name="valRecherche" value="Valider" class="col" >
  </form>
  </div>
  <div id="resu" class="offset-1">
  <div id="type"><a href="index.php?action=catalogueLivre">
  Livres
  </a>
  </div>
  <ul>    ';
  // affichage de la liste des livres, si il y en a enregistrés dans la bdd
  if(empty($l)){
    $res.="Aucun livre.";
  }else{
    foreach($l as $livre){
      $idLivre=$livre->id;
      $res.='<li class="span-3 col" id="'.$idLivre.'">
      <a href="index.php?action=documentDetail&id='.$idLivre.'"><span id="image"><img src="'.$livre->img.'"></span>
      <span id="infos">'.$livre->titre.'<br>'.$livre->auteur.'<br>'.$livre->date.'</span>
      </a></li>';
    }
  }

  $res.='</ul>
  <div id="aff_plus">
  <a href="index.php?action=catalogueLivre">Afficher tous</a>
  </div>

  <div id="type"><a href="index.php?action=catalogueCD">
  CDs
  </a> </div> 
  <ul>';
  // affichage de la liste des cd, si il y en a enregistrés dans la bdd
  if(empty($c)){
    $res.="Aucun CD.";
  }else{
    foreach($c as $cd){
      $idCD=$cd->id;
      $res.='<li class="span-3 col" id="'.$idCD.'">
      <a href="index.php?action=documentDetail&id='.$idCD.'"><span id="image"><img src="'.$cd->img.'"></span>
      <span id="infos">'.$cd->titre.'<br>'.$cd->auteur.'<br>'.$cd->date.'</span>
      </a></li>';
    }
  }
  $res.='</ul>
  <div id="aff_plus">
  <a href="index.php?action=catalogueCD">Afficher tous</a>
  </div>


  <div id="type"><a href="index.php?action=catalogueDVD">
  DVD
  </a></div>
  <ul> ';
  // affichage de la liste des dvd, si il y en a enregistrés dans la bdd
  if(empty($d)){
    $res.="Aucun DVD.";
  }else{
    foreach($d as $dvd){
      $idDVD=$dvd->id;
      $res.='<li class="span-3 col" id="'.$idDVD.'">
      <a href="index.php?action=documentDetail&id='.$idDVD.'"><span id="image"><img src="'.$dvd->img.'"></span>
      <span id="infos">'.$dvd->titre.'<br>'.$dvd->auteur.'<br>'.$dvd->date.'</span>
      </a></li>';
    }
  }  
  $res.='  </ul>
  <div id="aff_plus">
  <a href="index.php?action=catalogueDVD">Afficher tous</a>
  </div>
  </div>
  </div>';

  return $res;
}

public function affichWelcomeCatalogue(){
  $res='<div id="content" class="offset-1 span-10">
  <div id="welcome">
  <h3 class="centrer">Catalogue</h3>
  </div>';
  return $res;

}

public function affichWelcomeRecherche(){
  $res='<div id="content" class="offset-1 span-10">
  <div id="welcome">
  <h3 class="centrer">Recherche</h3>
  </div>';
  return $res;
}


public function afficheCatalogueLivre($l){
  $res="";
  $res.='<div id="content" class="offset-1 span-10">

  <div id="welcome">
  <h3 class="centrer">Catalogue des livres</h3>
  </div>

  <div id="search">
  <form action="index.php?action=resuRech" method="POST">
  <input type="text" name="mot_rech" id="mot_rech" placeholder="Recherche" class="offset-8 span-2 nomarginright">
  <input type="submit" class="btn" id="valRecherche" name="valRecherche" value="Valider" class="col" >
  </form>
  </div>

  <div id="resu" class="offset-1">
  <ul>    ';
  // affichage de la liste des livres, si il y en a enregistrés dans la bdd
  if(empty($l)){
    $res.="Aucun livre.";
  }else{
    foreach($l as $livre){
      $idLivre=$livre->id;
      $res.='<li class="span-3 col" id="'.$idLivre.'">
      <a href="index.php?action=documentDetail&id='.$idLivre.'"><span id="image"><img src="'.$livre->img.'"></span>
      <span id="infos">'.$livre->titre.'<br>'.$livre->auteur.'<br>'.$livre->date.'</span>
      </a></li>';
    }
  }

  $res.='</ul>';
  $res.='  </div></div>';

  return $res;
}

public function afficheCatalogueCD($c){
  $res="";
  $res.='<div id="content" class="offset-1 span-10">

  <div id="welcome">
  <h3 class="centrer">Catalogue des CDs</h3>
  </div>
  <div id="search">
  <form action="index.php?action=resuRech" method="POST">
  <input type="text" name="mot_rech" id="mot_rech" placeholder="Recherche" class="offset-8 span-2 nomarginright">
  <input type="submit" class="btn" id="valRecherche" name="valRecherche" value="Valider" class="col" >
  </form>
  </div>

  <div id="resu" class="offset-1">
  <ul>    ';
  // affichage de la liste des cds, si il y en a enregistrés dans la bdd
  if(empty($c)){
    $res.="Aucun CD.";
  }else{
    foreach($c as $cd){
      $idCD=$cd->id;
      $res.='<li class="span-3 nomarginright" id="'.$idCD.'"><a href="index.php?action=documentDetail&id='.$idCD.'">
      <span id="image"><img src="'.$cd->img.'"></span><br>
      <span id="titre">'.$cd->titre.'</span><br>
      <span id="auteur">'.$cd->auteur.'</span><br>
      <span id="annee">'.$cd->date.'</span>
      </li>';
    } 
  }

  $res.='</ul>';
  $res.='  </div></div>';

  return $res;
}


public function afficheCatalogueDVD($d){
  $res="";
  $res.='<div id="content" class="offset-1 span-10">

  <div id="welcome">
  <h3 class="centrer">Catalogue des DVD</h3>
  </div>

  <div id="search">
  <form action="index.php?action=resuRech" method="POST">
  <input type="text" name="mot_rech" id="mot_rech" placeholder="Recherche" class="offset-8 span-2 nomarginright">
  <input type="submit" class="btn" id="valRecherche" name="valRecherche" value="Valider" class="col" >
  </form>
  </div>
  <div id="resu" class="offset-1">
  <ul>    ';
  // affichage de la liste des dvd, si il y en a enregistrés dans la bdd
  if(empty($d)){
    $res.="Aucun DVD.";
  }else{
    foreach($d as $dvd){
      $idDVD=$dvd->id;
      $res.='<li class="span-3 nomarginright" id="'.$idDVD.'"><a href="index.php?action=documentDetail&id='.$idDVD.'">
      <span id="image"><img src="'.$dvd->img.'"></span><br>
      <span id="titre">'.$dvd->titre.'</span><br>
      <span id="auteur">'.$dvd->auteur.'</span><br>
      <span id="annee">'.$dvd->date.'</span>
      </li>';
    }  
  }  

  $res.='</ul>';
  $res.='  </div></div>';

  return $res;
}

public function affichGeneral($sel){

  echo $this->affichHeader();
  switch ($sel) {
    case 'accueil':
    echo $this->middle=$this->affichAccueil();
    break;

    case 'mesEmprunts':
    echo $this->affichNavbarDefaut();
    echo $this->middle = $this->affichMesEmprunts($this->emprunt);
    break;

    case 'catalogue':
    echo $this->affichNavbarDefaut();
    echo $this->middle=$this->affichWelcomeCatalogue();
    echo $this->middle = $this->afficheCatalogue($this->livre, $this->cd, $this->dvd);
    break;

    case 'documentDetail':
    echo $this->affichNavbarDefaut();
    echo $this->middle = $this->affichDocumentDetail($this->document, $this->genre, $this->etat,$this->emprunt);
    break;

    case 'formVoirEmprunts':
    echo $this->affichNavbarDefaut();
    echo $this->middle = $this->affichFormVoirMesEmprunts();
    break;

    case 'resultat':
    echo $this->affichNavbarDefaut();
    echo $this->middle=$this->affichWelcomeRecherche();
    echo $this->middle = $this->afficheCatalogue($this->livre, $this->cd, $this->dvd);
    break;

    case 'catalogueLivre':
    echo $this->affichNavbarDefaut();
    echo $this->middle = $this->afficheCatalogueLivre($this->livre);
    break;

    case 'catalogueCD':
    echo $this->affichNavbarDefaut();
    echo $this->middle = $this->afficheCatalogueCD($this->cd);
    break;

    case 'catalogueDVD':
    echo $this->affichNavbarDefaut();
    echo $this->middle = $this->afficheCatalogueDVD($this->dvd);
    break;
  }
  echo $this->affichFooter();

}


}

?>
