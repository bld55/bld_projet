<?php
	/**
	* 
	*/
	class C_GestionAdherent
	{
		
		function __construct()
		{

		}

		// affichage accueil
		public function actionAccueil($get){

			$v = new View();
			$v->affichGeneral($get['action']);

		}
		
		// genere affichage des emprunts
		public function actionMesEmprunts($get){
			$v = new View();
			$u = User::findByRef($_POST["id_adh"]);
			$id = $u->id_use;
			$d = Emprunt::findDocByUser($id);
			$v->emprunt=$d;
			$v->affichGeneral($get['action']);		
		}

		// genere affichage details de document (dont l'etat et le genre)
		public function actionDocumentDetail($get){
			$v = new View();
			$d = Document::findById($get['id']);
			$v->document=$d;
			$v->etat = Document::findEtatById($d->id_etat);
			$v->emprunt = Emprunt::findByIdDoc($d->id);
			$v->genre= Document::findGenre($d->id);
			$v->affichGeneral($get['action']);
		}

		/*	
			genere l'affichage du catalogue
			récupère la liste de documents correspondants au type sélectionné
		*/
		public function actionAfficheCatalogue($get){

			$v=new View();			
			$document=new Document();
			$v->livre=$document->findByType(1,4);
			$v->cd=$document->findByType(2,4);
			$v->dvd=$document->findByType(3,4);
			$v->affichGeneral('catalogue');
		}

		/*
			affiche le résultat de la recherche par type
		*/

		public function actionResultatRecherche($get){
			
			if(isset($_POST)){
				$v=new View();
				// input du formulaire 
				$mot=$_POST['mot_rech'];

				$document=new Document();
				$v->livre=$document->findAllLike($mot, 1);
				$v->cd=$document->findAllLike($mot, 2);
				$v->dvd=$document->findAllLike($mot, 3);
				$v->affichGeneral('resultat');
				
			}
		}

		// affiche le formulaire d'emprunt
		public function actionFormEmprunt($get){

			$v=new View();
			$v->affichGeneral($get['action']);
		}

		// affiche tout le catalogue des livres
		public function actionAfficheCatalogueLivre($get){

			$v=new View();			
			$document=new Document();
			$v->livre=$document->findByType(1);

			$v->affichGeneral('catalogueLivre');
		}

		// affiche tout le catalogue des CD
		public function actionAfficheCatalogueCD($get){

			$v=new View();			
			$document=new Document();
			$v->cd=$document->findByType(2);

			$v->affichGeneral('catalogueCD');
		}

		// affiche tout le catalogue des DVD
		public function actionAfficheCatalogueDVD($get){
			$v=new View();			
			$document=new Document();
			$v->dvd=$document->findByType(3);

			$v->affichGeneral('catalogueDVD');
		}



		public function dispatch($get){
			if(isset($get['action'])){
				switch ($get['action']) {

					case 'formVoirEmprunts':
					$this->actionFormEmprunt($get);
					break;

					case 'accueil':
					$this->actionAccueil($get);
					break;

					case 'documentDetail':
					$this->actionDocumentDetail($get);
					break;

					case 'catalogue':
					$this->actionAfficheCatalogue($get);
					break;

					case 'mesEmprunts':
					$this->actionMesEmprunts($get);
					break;

					case 'resuRech':
					$this->actionResultatRecherche($get);
					break;

					case 'catalogueLivre' :
					$this->actionAfficheCatalogueLivre($get);
					break;

					case 'catalogueCD':
					$this->actionAfficheCatalogueCD($get);
					break;

					case 'catalogueDVD':
					$this->actionAfficheCatalogueDVD($get);
					break;
				}
			}
			else{
				$this->actionAfficheCatalogue($get);
			}
		}
	}
