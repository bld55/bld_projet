<?php
class Doc_Genre{
	private $id_doc;
	private $id_gen;

	public function __construct(){
	}

	public function __toString() {
    return "[". __CLASS__ . "] id : ". $this->id . ":
    titre  ". $this->titre  .":
    description ". $this->description  ;
  }

  public function __get($attr_name) {
   if (property_exists( __CLASS__, $attr_name)) { 
    return $this->$attr_name;
  } 
  $emess = __CLASS__ . ": unknown member $attr_name (getAttr)";
  throw new Exception($emess, 45);
}

public function __set($attr_name, $attr_val) {
 if (property_exists( __CLASS__, $attr_name)) {
   $this->$attr_name=$attr_val; 
   return $this->$attr_name;
 } 
 $emess = __CLASS__ . ": unknown member $attr_name (setAttr)";
 throw new Exception($emess, 45);
}

// récupère les infos à propos du genre du document dont l'id est envoyé en paramètre
public static function findGenByIdDoc($id) {

  $pdo = Base::getConnection();

  $stmt = $pdo->prepare("select * from doc_genre where id_doc=:id");
  $stmt->bindParam(':id',$id);
  $stmt->execute();
  $dg=$stmt->fetchAll(PDO::FETCH_OBJ);

  $tr =array();
  foreach ($dg as $value) {
      	//var_dump($value);
   $doc_gen = new Doc_Genre();
   $doc_gen->id_doc=$value->id_doc;
   $doc_gen->id_gen=$value->id_gen;
   $tr[]=$doc_gen;
 }

 return $tr;

} 
}
?>