<?php
class Genre{
	private $id;
	private $libelle;

	public function __construct(){
	}

	public function __toString() {
    return "[". __CLASS__ . "] id : ". $this->id . ":
    titre  ". $this->titre  .":
    description ". $this->description  ;
  }

  public function __get($attr_name) {
   if (property_exists( __CLASS__, $attr_name)) { 
    return $this->$attr_name;
  } 
  $emess = __CLASS__ . ": unknown member $attr_name (getAttr)";
  throw new Exception($emess, 45);
}

public function __set($attr_name, $attr_val) {
 if (property_exists( __CLASS__, $attr_name)) {
   $this->$attr_name=$attr_val; 
   return $this->$attr_name;
 } 
 $emess = __CLASS__ . ": unknown member $attr_name (setAttr)";
 throw new Exception($emess, 45);
}

public static function findById($id) {

  $pdo = Base::getConnection();
  $stmt = $pdo->prepare("select * from genre where id_gen=:id");
  $stmt->bindParam(':id',$id);
  $stmt->execute();
  $d=$stmt->fetch(PDO::FETCH_OBJ);

  $gen = new Genre();
  $gen->id=$d->id_gen;
  $gen->libelle=$d->libelle_gen;

  return $gen;

} 
}
?>