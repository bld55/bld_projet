<?php
class User{
	private $id_use;
	private $nom_use;
	private $prenom_use;
	private $adresse_use;
	private $cp_use;
	private $ville_use;
	private $date_n;
	private $tel_use;
	private $mail_use;
  private $num_adh;

  public function __construct(){
  }

  public function __toString() {
    return "[". __CLASS__ . "] id : ". $this->id . ":
    titre  ". $this->titre  .":
    description ". $this->description  ;
  }

  public function __get($attr_name) {
   if (property_exists( __CLASS__, $attr_name)) { 
    return $this->$attr_name;
  } 
  $emess = __CLASS__ . ": unknown member $attr_name (getAttr)";
  throw new Exception($emess, 45);
}

public function __set($attr_name, $attr_val) {
 if (property_exists( __CLASS__, $attr_name)) {
   $this->$attr_name=$attr_val; 
   return $this->$attr_name;
 } 
 $emess = __CLASS__ . ": unknown member $attr_name (setAttr)";
 throw new Exception($emess, 45);
}

     public static function findAll() {


      $pdo = Base::getConnection();

      $stmt = $pdo->prepare('select * from user');
      $stmt->execute();
      $orow = $stmt->fetchAll(PDO::FETCH_OBJ);
      
      $tr = array();
      foreach ($orow as $row) {
        $o = new Document();
        $o->id_use = $row->id_use;
        $o->nom_use = $row->nom_use;
        $o->prenom = $row->prenom;
        $o->adresse = $row->adresse;
        $o->cp = $row->cp;
        $o->ville = $row->ville;
        $o->date_n = $row->date_n;
        $o->tel = $row->tel;
        $o->mail = $row->mail;
        $tr[]=$o;

      }
      return $tr;
    }

    // trouve les infos de l'utilisateur avec son numero d'adherent
    public static function findByRef($ref) {

      $pdo = Base::getConnection();
      $stmt = $pdo->prepare('select * from utilisateur,adherent where utilisateur.id_use = adherent.id_user and num_adh=:num_adh');
      $stmt->bindParam(":num_adh",$ref);
      $stmt->execute();
      $orow = $stmt->fetch(PDO::FETCH_OBJ);
      $u = new User();
      //echo var_dump($orow);
      if($orow!=false){
        
        $u->id_use=$orow->id_use;
        $u->nom_use=$orow->nom_use;
        $u->prenom_use=$orow->prenom_use;
        $u->adresse_use=$orow->adresse_use;
        $u->cp_use=$orow->cp_use;
        $u->ville_use=$orow->ville_use;
        $u->date_n=$orow->date_naiss_use;
        $u->tel_use=$orow->tel_use;
        $u->mail_use=$orow->mail_use;
        $u->num_adh=$orow->num_adh;
        $_SESSION['user']=$u;
      }      
      return $u;

    }
  }
  ?>