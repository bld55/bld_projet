<?php
class Emprunt{
	private $id_use;
	private $id_doc;
  private $id_emp;
  private $date_enr;
  private $date_retour;
  private $date_rendu;

  public function __construct(){
  }


  public function __get($attr_name) {
   if (property_exists( __CLASS__, $attr_name)) { 
    return $this->$attr_name;
  } 
  $emess = __CLASS__ . ": unknown member $attr_name (getAttr)";
  throw new Exception($emess, 45);
}

public function __set($attr_name, $attr_val) {
 if (property_exists( __CLASS__, $attr_name)) {
   $this->$attr_name=$attr_val; 
   return $this->$attr_name;
 } 
 $emess = __CLASS__ . ": unknown member $attr_name (setAttr)";
 throw new Exception($emess, 45);
}

// trouve tous les emprunts pour l'utilisateur dont l'id est passé en paramètres
public static function findDocByUser($id_user) {

  $pdo = Base::getConnection();

  $stmt = $pdo->prepare('select * from emprunt where id_use=:id_user');
  $stmt->bindParam(":id_user",$id_user);
  $stmt->execute();
  $allrow = $stmt->fetchAll(PDO::FETCH_OBJ);


  $tr = array();

  foreach ($allrow as $row) {

   $doc = Document::findById($row->id_doc);
   $o = new Document();
   $o->id = $doc->id;
   $o->ref = $doc->ref;
   $o->titre = $doc->titre;
   $o->auteur = $doc->auteur;

         // Affichage de l'année uniquement

   $date_doc=$doc->date;
   $date_doc=date('Y');
   $o->date = $date_doc;

   $o->desc = $doc->desc;
   $o->id_type = $doc->id_type;
   $o->id_etat = $doc->id_etat;
   $o->img = $doc->img;
   $tr[]=$o;


 }
 return $tr;
}

public static function findById($id) {

  $pdo = Base::getConnection();
  $stmt = $pdo->prepare("select * from emprunt where id_use=:id");
      //$stmt = $pdo->prepare("SELECT * FROM document, doc_genre, genre WHERE document.id_doc = doc_genre.id_doc AND doc_genre.id_gen=genre.id_gen AND document.id_doc=:id");
  $stmt->bindParam(':id',$id);
  $stmt->execute();
  $emprunt=$stmt->fetchAll(PDO::FETCH_OBJ);

  $tr = array();
  foreach ($emprunt as $emp) {
    $e = new Emprunt();
    $e->id_use=$emp->id_use;
    $e->id_doc=$emp->id_doc;
    $e->date_enr=$emp->date_enr;
    $e->date_retour=$emp->date_retour;
    $tr[] = $e; 
  }
  return $tr;
} 

// trouve les documents non rendu pour l'utilisateur dont l'id est passée en paramètres
public static function findByIdNonRendu($id){
  $pdo = Base::getConnection();
  $stmt = $pdo->prepare("SELECT COUNT(*) FROM emprunt WHERE id_use=:id AND date_retour IS NOT NULL");
      //$stmt = $pdo->prepare("SELECT * FROM document, doc_genre, genre WHERE document.id_doc = doc_genre.id_doc AND doc_genre.id_gen=genre.id_gen AND document.id_doc=:id");
  $stmt->bindParam(':id',$id);
  $stmt->execute();
  $rs=$stmt->fetchColumn();
  //echo var_dump($rs);
  return $rs;
}

// trouve dans les documents dans la table via l'id passée en paramètres
public static function findByIdDoc($id) {

  $pdo = Base::getConnection();
  $stmt = $pdo->prepare("select * from emprunt where id_doc=:id ORDER BY id_doc DESC LIMIT 1");
      //$stmt = $pdo->prepare("SELECT * FROM document, doc_genre, genre WHERE document.id_doc = doc_genre.id_doc AND doc_genre.id_gen=genre.id_gen AND document.id_doc=:id");
  $stmt->bindParam(':id',$id);
  $stmt->execute();
  $emprunt=$stmt->fetch(PDO::FETCH_OBJ);

  return $emprunt;
} 

public function insert() {

  $pdo = Base::getConnection();
  $stmt = $pdo->prepare("insert into emprunt (id_use,id_doc,date_enr,date_retour) values(?,?,?,?)");

  $id_use=$this->id_use;
  $id_doc=$this->id_doc;
  $date_enr=$this->date_enr;
  $date_retour=$this->date_retour;
  
  $stmt->bindParam(1,$id_use);
  $stmt->bindParam(2,$id_doc);
  $stmt->bindParam(3,$date_enr);
  $stmt->bindParam(4,$date_retour);


  $stmt->execute();

  $lastid=$pdo->LastInsertId('emprunt');
  $this->id_emp=$lastid;
}

public function update() {

  if (!isset($this->id_emp)) {
    throw new Exception(__CLASS__ . ": Primary Key undefined : cannot update");
  } 
  $save_query = 'UPDATE emprunt SET date_enr='.(isset($this->date_enr) ? '"'.$this->date_enr.'"' : 'null').',
  date_retour='.(isset($this->date_retour) ? '"'.$this->date_retour.'"' : 'null').',
  date_rendu='.(isset($this->date_rendu) ? '"'.$this->date_rendu.'"' : 'null').' where id_emp='.$this->id_emp.' and id_use='.$this->id_use.' and id_doc='.$this->id_doc;

  //echo $save_query;
  $pdo = Base::getConnection();
  $nb=$pdo->exec($save_query);

  return $nb;
}

// trouve les informations de l'emprunt via la référence du document
public static function findByRef($ref){
  $pdo= Base::getConnection();
  $stmt =$pdo->prepare("SELECT * FROM emprunt, document WHERE emprunt.id_doc=document.id_doc AND ref_doc=:ref");
  $stmt->bindParam(':ref', $ref);
  $stmt->execute();
  $rs=$stmt->fetch(PDO::FETCH_OBJ);
  if (is_object($rs)) {
    $e = new Emprunt();
    $e->id_emp=$rs->id_emp;
    $e->id_use=$rs->id_use;
    $e->id_doc=$rs->id_doc;
    $e->date_enr=$rs->date_enr;
    $e->date_retour=$rs->date_retour;
    $e->date_rendu=$rs->date_retour;
    $var=$e;
  }else{
    $var="";
  }
  return $var;
}


}
?>