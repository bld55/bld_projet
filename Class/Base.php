<?php

class Base {

	private static $dblink ;
	/* établie une connexion a la base et retourne une instance de PDO */

	private static function connect() {

		$config = parse_ini_file("config.ini");
		//var_dump($config);
		$user=$config['user'];
		$pass=$config['password'];
		$base=$config['base'];
		Try {
			$dsn="mysql:host=127.0.0.1;dbname=".$base;
			$db = new PDO($dsn, $user,$pass,
				array(PDO::ERRMODE_EXCEPTION=>true,
					PDO::ATTR_PERSISTENT=>true));
		} catch(PDOException $e) {
			throw new PDOException("connection error: $dsn".$e->getMessage(). '<br/>');
		}
		$db->exec("SET CHARACTER SET utf8");
		return $db;
		
	}

	/* vérifie si un lien existe le crée le cas échéant et le retourne */
	public static function getConnection() {
		
		if (isset(self::$dblink)) {
			return self::$dblink ;
		}else{
			self::$dblink = self::connect();
			return self::$dblink ;
		}
	}
}


?>