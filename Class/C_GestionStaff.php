<?php
	/**
	* 
	*/
	class C_GestionStaff
	{
		
		function __construct()
		{

		}

		public function actionEnrEmprunt($get){
			unset($_SESSION['errorad']);
			$vs = new ViewStaff();
			$vs->affichGeneral($get['action']);
		}

		public function actionNewEmprunt($get){
			unset($_SESSION['error']);
			unset($_SESSION['errorad']);
			$vs = new ViewStaff();

			if(empty($_POST)){
				$_SESSION['errorad']="Veuillez renseigner un numéro d'adhérent.";
				$get['action']='enremprunt';
			}else{
			// Cherche si le numAdh a été renseigné (ie si saisie dans la vue précédente)
				if(isset($_POST['numAdh'])){
					$u = User::findByRef($_POST['numAdh']);
					if(is_null($u->id_use)){
						$_SESSION['errorad']="L'adhérent n'existe pas.";
						header("Location: index.php?action=enremprunt");
					}else{
						$vs->user=$u;
					}
				}
				if (isset($_POST['enregistrer'])) {
				// Var u mise en session pour une réutilisation pour les prochaines pages
					$u=$_SESSION['user'];

					$d = Document::findByRefDoc($get['refinput']);
				// si ref correspond à un document enregistré dans la BDD
					if(is_object($d)){
					// si emprunté ou indisponible, message erreur
						if($d->id_etat!=1){
							$_SESSION['error']="Le document n'est pas disponible à l'emprunt.";
						}else
					// sinon modif de l'état, update du document dans la BDD
					// + création d'un nouvel emprunt
						{
							$d->id_etat=2;
							$d->update();
							$_SESSION['tr'][] = $d;

							$e = new Emprunt();
							$e->id_use=$u->id_use;
							$e->id_doc=$d->id;
							$e->date_enr=date("Y-m-d");

				    	//Calcul date retour
							$e->date_retour=date('Y-m-d',strtotime('+30 days'));
							$_SESSION['date_retour'] = $e->date_retour;
							$e->insert();

						}
				// si ref correspond pas à qque chose dans la BDD, message erreur
					}else{
						$_SESSION['error']="Le document n'a pu être trouvé.";
					}
				}
			}
			// si bouton terminé pressé
			if(isset($_POST['terminer'])){
				header("location: index.php?action=pagerecapenr");
			}
			$vs->affichGeneral($get['action']); 
		}


		public function actionPageRecapEnr($get){
			$vs = new ViewStaff();
			$vs->affichGeneral($get['action']);
			if(isset($_SESSION['tr'])){
				session_unset($_SESSION['tr']);
			}

		}

		public function actionRetour($get){
			unset($_SESSION['errorad']);
			unset($_SESSION['error']);
			$vs = new ViewStaff();

			if(isset($_POST['valRetour'])){
				$e = Emprunt::findByRef($_POST['retourDoc']);
				// si l'emprunt existe
				if (is_object($e)) {
					if(!is_null($e->id_emp)){
						try{
							$d = Document::findByRef($_POST['retourDoc']);

						}catch(Exception $e){
							print_r($e);
						}
						// si le document existe
						if(is_object($d)){
							// vérification de l'état, s'il est emprunté
							if($d->id_etat==2){
								// ajout dans la variable SESSION
								$_SESSION['recap_documents'][] = $d;

								// mise a jour de la bdd 
								$e->date_retour=null;
								$e->date_rendu=date('Y-m-d');
								$d->id_etat=1;

								$_SESSION['emprunt']=$e;

								$e->update();
								$d->update();

							}else{
								$_SESSION['error']="Le document est déjà rendu (Mauvaise référence ?).";
							}
						}else{
							$_SESSION['error']="L'emprunt est introuvable";	
						}
						
					}else{
						$_SESSION['error']="Le document est introuvable";
					}
					
				}else{
					$_SESSION['error']="L'emprunt est introuvable";
				}
			}
			// bouton terminer pressé
			if(isset($_POST['termRetour'])){
				$this->actionTermRetour();
			}else{
				$vs->affichGeneral($get['action']);
			}

		}

		// finalise le retour de documents
		public function actionTermRetour(){
			$vs = new ViewStaff();

			//echo var_dump($_SESSION);
			//echo var_dump($e);
			//Reprend le premier emprunt enregistré
			//echo var_dump($_SESSION);
			if (isset($_SESSION['emprunt'])) {
				$e=$_SESSION['emprunt'];
			}else{
				header("Location: index.php?action=retourEmprunt");
			}
			//$e=$_SESSION['emprunt'];
			//echo var_dump($e);
			//echo var_dump($_SESSION);

			$_SESSION['nbARendre']=$e->findByIdNonRendu($e->id_use);
			$vs->affichGeneral('termRetour');
			unset($_SESSION['nbARendre']);
		}



		public function dispatch($get){
			if(isset($get['action'])){
				switch ($get['action']) {

					case 'enremprunt':
					unset($_SESSION['user']);
					$this->actionEnrEmprunt($get);
					break;

					case 'newemprunt':
					$this->actionNewEmprunt($get);
					break;

					case 'retourEmprunt':
					$this->actionRetour($get);
					break;

					case 'pagerecapenr':
					$this->actionPageRecapEnr($get);
					break;
				}
			}else{
				$this->actionList($get);
			}
		}
	}
