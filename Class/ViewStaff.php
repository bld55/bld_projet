<?php

class ViewStaff {

 private $middle;

 private $document;

 private $livre;

 private $cd;

 private $dvd;

 private $emprunt;

 private $genre;

 private $user;

 private $doc_genre;

 private $echec;

 public function __construct() {

 }

 public function __get($attr_name) {
  if (property_exists( __CLASS__, $attr_name)) { 
    return $this->$attr_name;
  } 
  $emess = __CLASS__ . ": unknown member $attr_name (getAttr)";
  throw new Exception($emess, 45);
}

public function __set($attr_name, $attr_val) {
  if (property_exists( __CLASS__, $attr_name)) {
    $this->$attr_name=$attr_val; 
    return $this->$attr_name;
  } 
  $emess = __CLASS__ . ": unknown member $attr_name (setAttr)";
  throw new Exception($emess, 45);
}


// affiche le header de base 
public function affichHeader(){
  $resu='<!DOCTYPE html>
  <html>
  <head>
  <link href="css/grille1.css" rel="stylesheet" type="text/css" />
  <link href="css/framework_lj.css" rel="stylesheet" type="text/css" />
  <link href="css/style.css" rel="stylesheet" type="text/css" />
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <title>mediaNet - Nancy</title>
  </head>

  <body>
  <header>
  <div id="bandeau" class="col no_margin span-12 centrer uppercase">
  <h1>mediaNet <br><small>Médiathèque de la ville de Nancy</small></h1>
  </div>

  <div id="navbar" class="col span-12 sans_deco centrer">
  <strong>
  <ul class="no_padding ligne">
  <li class="sans_deco col span-6 no_margin">
  <a href="index.php?action=enremprunt">Nouvel Emprunt</a>
  </li>
  <li class="sans_deco col span-6 no_margin">
  <a href="index.php?action=retourEmprunt">Enregistrer un retour</a>
  </li>
  </ul>
  </strong>
  </div>
  </header>';
  return $resu;
}


public function affichEnrEmprunt($user){
  $resu='<div id="content" class="offset-1 span-10">
  <div id="welcome">
  <h3 class="centrer">Nouvel Emprunt</h3>
  </div>
  <div id="resu" class="centrer">
  <h4 class="centrer">N° d adhérent:</h4>
  <form method="POST" action="index.php?action=newemprunt">
  <input type="text" name="numAdh"/>
  <input type="submit" class="btn" value="valider">
  </form>
  <div class="error">';
  // si présence d'une erreur en session
  if(isset($_SESSION['errorad'])){
   $resu.= $_SESSION['errorad'];
 }
 $resu.='</div>
 </div>
 </div>';
 return $resu;
}

public function affichNewEmprunt($user){
  $resu='<div id="content" class="offset-1 span-10">
  <div id="welcome">
  <h3 class="centrer">Nouvel Emprunt</h3>
  </div>
  <div id="resu" class="centrer">';
  // affichage des infos de l'utilisateur
  $resu.='<h4 class="centrer">N° d adhérent:</h4>  '.$_SESSION['user']->num_adh.'
  <h4 class="centrer">Infos client: </h4> '.$_SESSION['user']->nom_use.' '.$_SESSION['user']->prenom_use.' '.$_SESSION['user']->date_n.'
  <form name="recherche_emprunt" method="POST" action="index.php?action=newemprunt">
  <h4 class="centrer">Référence(s): </h4>
  <input type="text" name="refinput"/></br>
  <input type="hidden" value="'.$_SESSION['user']->id_use.'" name="hiddenid_use"/>
  <input type="submit" class="btn" name="enregistrer" value="Enregistrer"/>
  <input type="submit" class="btn" name="terminer" value="Terminer"/>
  </form>
  <div class="error">';
  // affichage de potentiels erreur en session
  if(isset($_SESSION['error'])){
   $resu.= $_SESSION['error'];
 }
 $resu.='</div>

 </div>
 </div>';
 return $resu;
}


public function affichPageRecapEnr(){

  $resu='<div id="content" class="offset-1 span-10">
  <div id="welcome">
  <h3 class="centrer">Nouvel Emprunt</h3>
  </div>';

  // si la session tr existe
  if(isset($_SESSION['tr'])){
    $resu.='<div id="resu" class="centrer">
    <h4 class="centrer">N° d adhérent:</h4>  '.$_SESSION['user']->num_adh.'
    <h4 class="centrer">Infos client: </h4> '.$_SESSION['user']->nom_use.' '.$_SESSION['user']->prenom_use.' '.$_SESSION['user']->date_n.'
    <h4 class="centrer">Documents empruntés:</h4> 
    <ul>';

    $e = $_SESSION['date_retour'];
    foreach($_SESSION['tr'] as $t){

      $resu.='<li class="span-3 nomarginright">
      <span id="image"><img src="'.$t->img.'"></span><br>
      <span id="titre">'.$t->titre.'</span><br>
      <span id="auteur">'.$t->auteur.'</span><br>
      <span id="annee">'.$t->date.'</span>
      </li>';
    }
    $resu.='
    </ul>
    La date de retour est prévue pour le '.$_SESSION['date_retour'].'. Merci de bien rendre le(s) document(s) à temps.
    </div>
    </div>
    </div>';
  }else{
    $resu.='<p class="centrer">ERREUR retourner sur accueil</p></div>';
  }
  return $resu;
}



public function affichRetour(){
  $res="<div id='content' class='offset-1 span-10'>
  <div id='welcome' class='centrer'>
  <h3>Retour Emprunt</h3>
  </div>

  <div id='retourEmprunt' class='offset-1 span-10 centrer'>
  <form id='retour' method='POST' action='index.php?action=retourEmprunt'>
  <strong>
  Reférence du document : 
  </strong>
  <input type='text' name='retourDoc' id='retourDoc'>
  <input type='submit' class='btn' name='valRetour' id='valRetour' value='Valider'>
  <input type='submit' class='btn' name='termRetour' id='termRetour' value='Terminer'>
  </form>
  <div class='error'>";
  // affichage d'eventuelles erreur
  if(isset($_SESSION['error'])){
    $res.= $_SESSION['error'];
  }
  if(isset($_SESSION['errorad'])){
    $res.= $_SESSION['errorad'];
  }
  $res.="</div></div>
  </div>
  </div>";
  return $res;
}

public function affichTermRetour($tab=null){
  $res="<div id='content' class='offset-1 span-10'>
  <div id='welcome' class='centrer'>
  <h3>Recapitulatif des retour d'emprunts</h3>

  </div>

  <div id='resu' class='offset-1'>";
  // compte le nb de documents rendus
  $total = count($tab);
  if($total==1 || $total==0){
    $res.="<strong class='centrer'>".$total." document a été rendu.</strong></br>";
  }else{
    $res.="<strong class='centrer'>".$total." documents ont été rendus.</strong></br>";
  }
  // affiche le nb de documents à rendre
  if($_SESSION['nbARendre']==0){
    $res.="<strong>Vous n'avez plus de documents à rendre.</strong>";
  }else{
    if($_SESSION['nbARendre']==1){
      $res.="<strong>Il vous reste ".$_SESSION['nbARendre']." document à rendre.</strong>";
    }else{
      $res.="<strong>Il vous reste ".$_SESSION['nbARendre']." documents à rendre.</strong>";
    }
  }
  // affiche le tableau recapitulatif des documents rendus
  $res.="<ul>";
  if(isset($tab)){
    foreach($tab as $t){
      $res.='<li class="span-3 nomarginright">
      <span id="image"><img src="'.$t->img.'"></span><br>
      <span id="titre">'.$t->titre.'</span><br>
      <span id="auteur">'.$t->auteur.'</span><br>';
      $date = date_parse($t->date);
      $res.='<span id="annee">'.$date['year'].'</span>
      </li>';

    }
    $res.='</div></div>';
  }
  return $res;
}

// affiche diverses erreurs
public function affichError(){
  echo "<div id='content' class='offset-1 span-10'>
  <div id='welcome' class='centrer'>
  <h3>Erreur</h3>
  </div>

  <div class='centrer'>
  <p>Une erreur s'est produite.</p>
  <a href='index.php'>Retour à l'accueil</a>
  </div>
  </div>";
}

public function affichFooter(){
  $resu="</body>";
  return $resu;
}

public function affichGeneral($sel){

  echo $this->affichHeader();
  switch ($sel) {

    case 'newemprunt':
    echo $this->middle = $this->affichNewEmprunt($this->user);
    break;

    case 'enremprunt':
    echo $this->middle = $this->affichEnrEmprunt($this->user);
    break;

    case 'pagerecapenr':
    echo $this->middle = $this->affichPageRecapEnr();
    break;

    case 'retourEmprunt':
    echo $this->middle = $this->affichRetour();
    break;

    case 'termRetour':
    if(isset($_SESSION['recap_documents'])){
      echo $this->middle = $this->affichTermRetour($_SESSION['recap_documents']);
    }else{
      echo $this->middle = $this->affichError();
    }
    
    unset($_SESSION['recap_documents']);
    unset($_SESSION['nbARendre']);
    break;
  }
  echo $this->affichFooter();

}
}