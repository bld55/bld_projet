<?php
class Document{
	private $id;
	private $ref;
	private $titre;
	private $auteur;
	private $date;
	private $desc;
  private $img;
  private $id_type;
  private $id_etat;

  public function __construct(){
  }

  public function __toString() {
    return "[". __CLASS__ . "] id : ". $this->id . ":
    titre  ". $this->titre  .":
    description ". $this->description  ;
  }

  public function __get($attr_name) {
   if (property_exists( __CLASS__, $attr_name)) { 
    return $this->$attr_name;
  } 
  $emess = __CLASS__ . ": unknown member $attr_name (getAttr)";
  throw new Exception($emess, 45);
}

public function __set($attr_name, $attr_val) {
 if (property_exists( __CLASS__, $attr_name)) {
   $this->$attr_name=$attr_val; 
   return $this->$attr_name;
 } 
 $emess = __CLASS__ . ": unknown member $attr_name (setAttr)";
 throw new Exception($emess, 45);
}

public function update() {

  if (!isset($this->id)) {
    throw new Exception(__CLASS__ . ": Primary Key undefined : cannot update");
  } 
  $save_query = 'UPDATE document SET ref_doc='.(isset($this->ref) ? '"'.$this->ref.'"' : 'null').',
  titre_doc='.(isset($this->titre) ? '"'.$this->titre.'"' : 'null').',
  auteur_doc='.(isset($this->auteur) ? '"'.$this->auteur.'"' : 'null').',
  date_doc='.(isset($this->date) ? '"'.$this->date.'"' : 'null').',
  desc_doc='.(isset($this->desc) ? '"'.$this->desc.'"' : 'null').',
  img_doc='.(isset($this->img) ? '"'.$this->img.'"' : 'null').',
  id_type='.(isset($this->id_type) ? '"'.$this->id_type.'"' : 'null').',
  id_etat='.(isset($this->id_etat) ? '"'.$this->id_etat.'"' : 'null').' where id_doc='.$this->id;

  //echo $save_query;
  $pdo = Base::getConnection();
  $nb=$pdo->exec($save_query);

  return $nb;
}

        public static function findAll() {

          $pdo = Base::getConnection();

          $stmt = $pdo->prepare('select * from document');
          $stmt->execute();
          $orow = $stmt->fetchAll(PDO::FETCH_OBJ);
          
          $tr = array();
          foreach ($orow as $row) {
            $o = new Document();
            $o->id = $row->id;
            $o->ref = $row->ref;
            $o->titre = $row->titre;
            $o->auteur = $row->auteur;
            $o->date = $row->date;
            $o->desc = $row->desc;
            $o->id_type = $row->id_type;
            $o->id_etat = $row->id_etat;
            $tr[]=$o;

          }
          return $tr;
        }

  public static function findById($id) {

    $pdo = Base::getConnection();
    $stmt = $pdo->prepare("select * from document where id_doc=:id");
      //$stmt = $pdo->prepare("SELECT * FROM document, doc_genre, genre WHERE document.id_doc = doc_genre.id_doc AND doc_genre.id_gen=genre.id_gen AND document.id_doc=:id");
    $stmt->bindParam(':id',$id);
    $stmt->execute();
    $d=$stmt->fetch(PDO::FETCH_OBJ);

    $doc = new Document();
    $doc->id=$d->id_doc;
    $doc->ref=$d->ref_doc;
    $doc->titre=$d->titre_doc;
    $doc->auteur=$d->auteur_doc;
    $doc->date=$d->date_doc;
    $doc->desc=$d->desc_doc;
    $doc->img=$d->img_doc;
    $doc->id_type=$d->id_type;
    $doc->id_etat=$d->id_etat;

    return $doc;
  }

  // recherche par type (livre, cd, dvd) avec une limite ou non
  public static function findByType($id, $max=null){
    $pdo = Base::getConnection();
    if(is_null($max)){
      $q='select * from document where id_type=:id';
    }else{
      $q='select * from document where id_type=:id LIMIT 4';
    }
    $stmt = $pdo->prepare($q);

    $stmt->bindParam(":id",$id);

    $stmt->execute();
    $orow = $stmt->fetchAll(PDO::FETCH_OBJ);

    $tr= array();
    foreach ($orow as $row) {
      $o = new Document();
      $o->id = $row->id_doc;
      $o->ref = $row->ref_doc;
      $o->titre = $row->titre_doc;
      $o->auteur = $row->auteur_doc;

        // Affichage de l'année uniquement
      $date = date_parse($row->date_doc);
      $o->date=$date['year'];

      $o->desc = $row->desc_doc;
      $o->img = $row->img_doc;
      $o->id_type = $row->id_type;
      $o->id_etat = $row->id_etat;
      $tr[]=$o;
    }
    return $tr;
  }

  // recherche tous les documents où le paramètre mot correspond à l'un des champs de la table
  public function findAllLike($mot, $type){
    $pdo = Base::getConnection();
    $stmt = $pdo->prepare("SELECT * FROM document WHERE (`ref_doc` LIKE CONCAT('%', :mot, '%') OR `titre_doc` LIKE CONCAT('%', :mot, '%') OR `auteur_doc` LIKE CONCAT('%', :mot, '%') OR `date_doc` LIKE CONCAT('%', :mot, '%') OR `desc_doc` LIKE CONCAT('%', :mot, '%')) AND `id_type`=:type");
    $stmt->bindParam(':mot', $mot);
    $stmt->bindParam(':type', $type);
    $stmt->execute();
      //echo var_dump($stmt->fetchAll(PDO::FETCH_OBJ));

      //$orow = $stmt->fetchAll(PDO::FETCH_OBJ);

    $tr= array();
    while ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
      $o = new Document();
      $o->id = $row->id_doc;
      $o->ref = $row->ref_doc;
      $o->titre = $row->titre_doc;
      $o->auteur = $row->auteur_doc;

        // Affichage de l'année uniquement
      $date = date_parse($row->date_doc);
      $o->date=$date['year'];

      $o->desc = $row->desc_doc;
      $o->img = $row->img_doc;
      $o->id_type = $row->id_type;
      $o->id_etat = $row->id_etat;
      $tr[]=$o;
    }
      //echo var_dump($tr);
    return $tr;
  }

  // trouver l'état en fonction du parametre
  public static function findEtatById($id){

    $pdo = Base::getConnection();
    $stmt = $pdo->prepare('select id_etat, libelle_etat from etat where id_etat=:id_etat');
    $stmt->bindParam(":id_etat",$id);
    $stmt->execute();
    $orow = $stmt->fetch(PDO::FETCH_OBJ);
    return $orow;

  }

  //trouver le document en fonction du numéro de référence
  public static function findByRef($ref){
    if (isset($ref)) {
      
      $pdo= Base::getConnection();
      $stmt =$pdo->prepare("SELECT * FROM emprunt, document WHERE emprunt.id_doc=document.id_doc AND ref_doc=:ref");
      $stmt->bindParam(':ref', $ref);
      $stmt->execute();
      $rs=$stmt->fetch(PDO::FETCH_OBJ);

      $d = new Document();
      $d->id=$rs->id_doc;
      $d->ref = $rs->ref_doc;
      $d->titre = $rs->titre_doc;
      $d->auteur = $rs->auteur_doc;
      $d->date = $rs->date_doc;
      $d->desc = $rs->desc_doc;
      $d->img = $rs->img_doc;
      $d->id_type = $rs->id_type;
      $d->id_etat = $rs->id_etat;

    }else{
      throw new Exception("Error Processing Request", 1);
    }

    return $d;
  }

  public static function findByRefDoc($ref){

    $pdo= Base::getConnection();
    $stmt =$pdo->prepare("select * from document where ref_doc=:ref");
    $stmt->bindParam(':ref', $ref);
    $stmt->execute();
    $rs=$stmt->fetch(PDO::FETCH_OBJ);

    //echo var_dump($rs);
    if($rs==false){
      return "Reférence erronée";
    }else{
      $d = new Document();
      $d->id=$rs->id_doc;
      $d->ref = $rs->ref_doc;
      $d->titre = $rs->titre_doc;
      $d->auteur = $rs->auteur_doc;
      $d->date = $rs->date_doc;
      $d->desc = $rs->desc_doc;
      $d->img = $rs->img_doc;
      $d->id_type = $rs->id_type;
      $d->id_etat = $rs->id_etat;
      return $d;
    }

  }

  // trouve le libelle du/des genres du document à partir de son id
  public static function findGenre($id){
    $pdo=Base::getConnection();
    $stmt = $pdo->prepare("SELECT libelle_gen FROM document, doc_genre, genre WHERE document.id_doc=doc_genre.id_doc AND doc_genre.id_gen = genre.id_gen AND document.id_doc=:id");
    $stmt->bindParam(':id',$id);
    $stmt->execute();
    $rs=$stmt->fetchAll(PDO::FETCH_OBJ);

    return $rs;
  }
}
?>
